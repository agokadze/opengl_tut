#version 330 core
in vec3 ourColor;

out vec4 color;

uniform float scale;

void main()
{
    color = vec4(ourColor, 1.0f) * ((scale / 3) + 0.65);
}