#version 330 core
out vec4 color;
  
uniform vec3 objectColor;
uniform vec3 lightColor;

void main()
{
	float ambient = 0.5;
    color = vec4(lightColor * objectColor, 1.0f) * ambient;
}