#pragma once

#include <string>
#include <vector>
#include <GL/glew.h>
#include <glm/glm.hpp>
#include <assimp/Importer.hpp>
#include <Shader.h>

/* A mesh should at least need a set of vertices where each vertex contains a position vector, a normal vector and a texture coordinate vector. A mesh should also contain indices for indexed drawing and material data in the form of textures (diffuse/specular maps).*/
//We store each of the required vectors in a struct called Vertex that we can use to index each of the vertex attributes. 
struct Vertex
{
	glm::vec3 Position;
	glm::vec3 Normal;
	glm::vec2 TexCoords;
	glm::vec3 Tangents;
};

struct Texture
{
	GLuint id;
	std::string type; //a diffuse texture or a specular texture
	aiString path; // We store the path of the texture to compare with other textures
};

class Mesh
{
public:
	//Mesh data
	std::vector<Vertex> vertices;
	std::vector<GLuint> indices;
	std::vector<Texture> textures;
	GLuint VAO;
	bool loadTangents;

	//Functions
	//Constructor
	Mesh(std::vector<Vertex> &vertices, std::vector<GLuint> &indices, std::vector<Texture> &textures, bool loadTangents = false);
	void Draw(Shader &shader);// Render the mesh
	void Draw_Ins(Shader &shader, GLuint amount);
private:
	//Render data
	GLuint VBO, EBO;
	//Functions
	void setupMesh();// Initializes all the buffer objects/arrays
};