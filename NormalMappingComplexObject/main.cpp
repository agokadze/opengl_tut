#include <iostream>
#include <GL/glew.h>

// GLFW
#include <GLFW/glfw3.h>

//SOIL
#include <SOIL/SOIL.h>

//GLM
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <shader.h>
#include <camera.h>
#include "Model.h"

#pragma region GlobalVariables//////////////////////////////////////////////////////////
const GLuint windowWidth = 800, windowHeigth = 600; //Window dimensions

GLuint planeVAO = 0, planeVBO; // quadVAO = 0, quadVBO;// , cubeVAO = 0, cubeVBO = 0;

bool keys[1024]; //keys pressed/released

GLfloat deltaTime = 0.0f; //Time between current frame and last frame
GLfloat lastFrame = 0.0f; //Time of last frame

GLfloat lastX = windowWidth / 2, lastY = windowHeigth / 2; //last mouse position

bool firstMouse = true; //is the first time we receive mouse input 

Camera camera(glm::vec3(0.0f, 0.0f, 3.0f));

bool normalMapping = true, inTangentSpace = true;

#pragma endregion GlobalVariables///////////////////////////////////////////////////////

#pragma region FunctionsDeclarations//////////////////////////////////////////////////////////
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);
void do_movement();
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
GLuint loadTexture(GLchar* path);
void RenderPlane();
#pragma endregion FunctionsDeclarations///////////////////////////////////////////////////////
int main()
{
#pragma region Initialization
	glfwInit(); //Initializes the GLFW library. Before most GLFW functions can be used. Returns GL_TRUE if succesfull
	//Set hints for the next call to glfwCreateWindow.  i.e. configure GLFW
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); //specify the client API version that the created context must be compatible with
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3); //specify the client API version that the created context must be compatible with
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); //specifies which OpenGL profile to create the context forspecifies whether the windowed mode window will be resizable by the user
	glfwWindowHint(GLFW_RESIZABLE, GL_FALSE); //specifies whether the windowed mode window will be resizable by the user
	//glfwCreateWindow creates a window and its associated context
	GLFWwindow* window = glfwCreateWindow(windowWidth, windowHeigth, "LearnOpenGL",
		nullptr, // screen mode, NULL - windowed mode
		nullptr //the window whose context to share										resources with
		);
	if (window == nullptr)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate(); //This function destroys all remaining windows and cursors, restores any modified gamma ramps and frees any other allocated resources. 
		return -1;
	}
	glfwMakeContextCurrent(window); //This function makes the OpenGL or OpenGL ES context of the specified window current on the calling thread.

	glewExperimental = GL_TRUE; //Setting glewExperimental to true ensures GLEW uses more modern techniques for managing OpenGL functionality. Set this to true so GLEW knows to use a modern approach to retrieving function pointers and extensions Ensures that all extensions with valid entry points will be exposed
	if (glewInit() != GLEW_OK) // Initialize GLEW to setup the OpenGL Function pointers
	{
		std::cout << "Failed to initialize GLEW" << std::endl;
		return -1;
	}

	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED); //hides and grabs cursor
	glfwSetWindowPos(window, 500, 100);

	glfwSetKeyCallback(window, key_callback); //sets the key callback of the specified window, which is called when a key is pressed, repeated or released
	glfwSetCursorPosCallback(window, mouse_callback); // sets the cursor position callback of the specified window, which is called when the cursor is moved. The callback is provided with the position, in screen coordinates, relative to the upper-left corner of the client area of the window.
	glfwSetScrollCallback(window, scroll_callback); //sets the scroll callback of the specified window, which is called when a scrolling device is used, such as a mouse wheel or scrolling area of a touchpad.

	glViewport(0, 0, windowWidth, windowHeigth); //specifies the actual window rectangle for your renderings, if y=0, it's at the bottom of the viewport

	glEnable(GL_DEPTH_TEST); //enables depth testing
#pragma endregion Initialization//////////////////////////////////////////////////////////

	// Build and compile our shader program
	Shader NMShader("normal_mapping.vert", "normal_mapping.frag");
	Model cyborg("../Models/cyborg/cyborg.obj");
	glm::vec3 lightPosition(0.0f, 3.0f, 0.9f); //light source's location in world-space coordinates
#pragma region DefinitionVBO&VAO//////////////////////////////////////////////////////////

#pragma endregion DefinitionVBO&VAO///////////////////////////////////////////////////////

#pragma region GeneratingTextures/////////////////////////////////////////////////////////
	//GLuint diffuseMap = loadTexture("../../../resources/brickwall.jpg");
	//GLuint normalMap = loadTexture("../../../resources/brickwall_normal.jpg");

#pragma endregion GeneratingTextures//////////////////////////////////////////////////////	

#pragma region ConfigureFBO/////////////////////////////////////////////////////////

#pragma endregion ConfigureFBO//////////////////////////////////////////////////////

#pragma region CalculateStaticMatrices////////////////////////////////////////////////////
	glm::mat4 NMShader_modelMatrix, NMShader_modelMatrix_lightSource;
	NMShader_modelMatrix_lightSource = glm::translate(glm::mat4(), lightPosition);
	NMShader_modelMatrix_lightSource = glm::scale(NMShader_modelMatrix_lightSource, glm::vec3(0.1f));
	//NMShader_modelMatrix = glm::rotate(NMShader_modelMatrix, glm::radians(90.0f), glm::vec3(0.0f, 1.0f, 0.0f));
#pragma endregion CalculateStaticMatrices/////////////////////////////////////////////////

#pragma region SetStaticUniforms//////////////////////////////////////////////////////////
	NMShader.Use();
	//glUniform1i(glGetUniformLocation(NMShader.Program, "diffuseMap"), 0);
	//glUniform1i(glGetUniformLocation(NMShader.Program, "normalMap"), 1);
	glUniform3fv(glGetUniformLocation(NMShader.Program, "lightPosition"), 1, glm::value_ptr(lightPosition));

#pragma endregion SetStaticUniforms///////////////////////////////////////////////////////

#pragma region GetDynamicUniformLocations/////////////////////////////////////////////////
	GLuint NMShader_modelMatrix_Loc = glGetUniformLocation(NMShader.Program, "modelMatrix");
	GLuint NMShader_projMatrix_Loc = glGetUniformLocation(NMShader.Program, "projectionMatrix");
	GLuint NMShader_viewMatrix_Loc = glGetUniformLocation(NMShader.Program, "viewMatrix");
	GLuint NMShader_cameraPosition_Loc = glGetUniformLocation(NMShader.Program, "cameraPosition");
	GLuint NMShader_normalMapping_Loc = glGetUniformLocation(NMShader.Program, "normalMapping");
	GLuint NMShader_inTangentSpace_Loc = glGetUniformLocation(NMShader.Program, "inTangentSpace");
#pragma endregion GetDynamicUniformLocations//////////////////////////////////////////////

#pragma region Game Loop//////////////////////////////////////////////////////////////////
	while (!glfwWindowShouldClose(window)) //while close flag of specified window is false. While window hasn't been instructed to close
	{
		GLfloat currentFrame = glfwGetTime();
		deltaTime = currentFrame - lastFrame;  //the time it takes to render the last frame
		lastFrame = currentFrame;
		//countFPS(currentFrame);
		glfwPollEvents(); //This function processes only those events that are already in the event queue and then returns immediately.Processing events will cause the window and input callbacks associated with those events to be called.
		do_movement();
#pragma region Rendering//////////////////////////////////////////////////////////////////
		glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		NMShader.Use();
		glm::mat4 viewMatrix = camera.GetViewMatrix();
		glm::mat4 projMatrix = glm::perspective(camera.Zoom, (GLfloat)windowWidth / (GLfloat)windowHeigth, 0.1f, 100.0f);
		glUniformMatrix4fv(NMShader_viewMatrix_Loc, 1, GL_FALSE, glm::value_ptr(viewMatrix));
		glUniformMatrix4fv(NMShader_projMatrix_Loc, 1, GL_FALSE, glm::value_ptr(projMatrix));
		// Render normal-mapped quad
		//NMShader_modelMatrix = glm::rotate(glm::mat4(), glm::radians((GLfloat)glfwGetTime()*-10), glm::normalize(glm::vec3(1.0f, 0.0f, 1.0f))); // Rotates the quad to show normal mapping works in all directions
		glUniformMatrix4fv(NMShader_modelMatrix_Loc, 1, GL_FALSE, glm::value_ptr(NMShader_modelMatrix));
		glUniform3fv(NMShader_cameraPosition_Loc, 1, glm::value_ptr(camera.Position));
		glUniform1i(NMShader_normalMapping_Loc, normalMapping);
		glUniform1i(NMShader_inTangentSpace_Loc, inTangentSpace);

		cyborg.Draw(NMShader);

		// render light source (simply re-renders a smaller plane at the light's position for debugging/visualization)
		glUniformMatrix4fv(NMShader_modelMatrix_Loc, 1, GL_FALSE, glm::value_ptr(NMShader_modelMatrix_lightSource));
		// Render Depth map to quad
		RenderPlane();
		//glBindTexture(GL_TEXTURE_2D, 0);
#pragma endregion MainShader////////////////////////////////////////////////////////////////

#pragma endregion Rendering///////////////////////////////////////////////////////////////
		glfwSwapBuffers(window); //swaps the front and back buffers of the specified window
	}
#pragma endregion Game Loop//////////////////////////////////////////////////////////////

#pragma region De-allocation//////////////////////////////////////////////////////////////
	glBindVertexArray(0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	//glDeleteTextures(1, &diffuseMap);
	//glDeleteTextures(1, &normalMap);
#pragma endregion De-allocation//////////////////////////////////////////////////////////////
	glfwTerminate(); //Terminate GLFW. This function destroys all remaining windows and cursors, restores any modified gamma ramps and frees any other allocated resources. 
	return 0;
}

void RenderPlane()
{
	if (planeVAO == 0)
	{
		//Positions
		glm::vec3 position1(-1.0f, 1.0f, 0.0f);
		glm::vec3 position2(-1.0f, -1.0f, 0.0f);
		glm::vec3 position3(1.0f, -1.0f, 0.0f);
		glm::vec3 position4(1.0f, 1.0f, 0.0f);
		//Texture coordinates
		glm::vec2 uv1(0.0, 1.0);
		glm::vec2 uv2(0.0, 0.0);
		glm::vec2 uv3(1.0, 0.0);
		glm::vec2 uv4(1.0, 1.0);
		//normal vector
		glm::vec3 nm(0.0f, 0.0f, 1.0f);

		// calculate tangent/bitangent vectors of both triangles
		glm::vec3 tangent1, tangent2, bitangent1, bitangent2;
		// - triangle 1
		glm::vec3 edge1 = position2 - position1;
		glm::vec3 edge2 = position3 - position1;
		glm::vec2 deltaUV1 = uv2 - uv1;
		glm::vec2 deltaUV2 = uv3 - uv1;

		GLfloat f = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y);

		tangent1.x = f * (deltaUV2.y*edge1.x - deltaUV1.y*edge2.x);
		tangent1.y = f * (deltaUV2.y*edge1.y - deltaUV1.y*edge2.y);
		tangent1.z = f * (deltaUV2.y*edge1.z - deltaUV1.y*edge2.z);
		tangent1 = glm::normalize(tangent1);

		bitangent1.x = f * (-deltaUV2.x*edge1.x + deltaUV1.x*edge2.x);
		bitangent1.y = f * (-deltaUV2.x*edge1.y + deltaUV1.x*edge2.y);
		bitangent1.z = f * (-deltaUV2.x*edge1.z + deltaUV1.x*edge2.z);
		bitangent1 = glm::normalize(bitangent1);

		// - triangle 2
		edge1 = position3 - position1;
		edge2 = position4 - position1;
		deltaUV1 = uv3 - uv1;
		deltaUV2 = uv4 - uv1;

		f = 1.0f / (deltaUV1.x * deltaUV2.y - deltaUV2.x * deltaUV1.y);

		tangent2.x = f * (deltaUV2.y*edge1.x - deltaUV1.y*edge2.x);
		tangent2.y = f * (deltaUV2.y*edge1.y - deltaUV1.y*edge2.y);
		tangent2.z = f * (deltaUV2.y*edge1.z - deltaUV1.y*edge2.z);
		tangent2 = glm::normalize(tangent2);

		bitangent2.x = f * (-deltaUV2.x*edge1.x + deltaUV1.x*edge2.x);
		bitangent2.y = f * (-deltaUV2.x*edge1.y + deltaUV1.x*edge2.y);
		bitangent2.z = f * (-deltaUV2.x*edge1.z + deltaUV1.x*edge2.z);
		bitangent2 = glm::normalize(bitangent2);

		GLfloat planeVertices[] = {
			// Positions       // Normals          // Texture Coords 
			position1.x, position1.y, position1.z, nm.x, nm.y, nm.z, uv1.x, uv1.y, tangent1.x, tangent1.y, tangent1.z, bitangent1.x, bitangent1.y, bitangent1.z,
			position2.x, position2.y, position2.z, nm.x, nm.y, nm.z, uv2.x, uv2.y, tangent1.x, tangent1.y, tangent1.z, bitangent1.x, bitangent1.y, bitangent1.z,
			position3.x, position3.y, position3.z, nm.x, nm.y, nm.z, uv3.x, uv3.y, tangent1.x, tangent1.y, tangent1.z, bitangent1.x, bitangent1.y, bitangent1.z,

			position1.x, position1.y, position1.z, nm.x, nm.y, nm.z, uv1.x, uv1.y, tangent2.x, tangent2.y, tangent2.z, bitangent2.x, bitangent2.y, bitangent2.z,
			position3.x, position3.y, position3.z, nm.x, nm.y, nm.z, uv3.x, uv3.y, tangent2.x, tangent2.y, tangent2.z, bitangent2.x, bitangent2.y, bitangent2.z,
			position4.x, position4.y, position4.z, nm.x, nm.y, nm.z, uv4.x, uv4.y, tangent2.x, tangent2.y, tangent2.z, bitangent2.x, bitangent2.y, bitangent2.z
		};

		glGenVertexArrays(1, &planeVAO);
		glGenBuffers(1, &planeVBO);
		glBindVertexArray(planeVAO);
		glBindBuffer(GL_ARRAY_BUFFER, planeVBO);
		glBufferData(GL_ARRAY_BUFFER, sizeof(planeVertices), &planeVertices, GL_STATIC_DRAW);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 14 * sizeof(GLfloat), (GLvoid*)0);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 14 * sizeof(GLfloat), (GLvoid*)(3 * sizeof(GLfloat)));
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 14 * sizeof(GLfloat), (GLvoid*)(6 * sizeof(GLfloat)));
		glEnableVertexAttribArray(2);
		glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, 14 * sizeof(GLfloat), (GLvoid*)(8 * sizeof(GLfloat)));
		glEnableVertexAttribArray(3);
		glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, 14 * sizeof(GLfloat), (GLvoid*)(11 * sizeof(GLfloat)));
		glEnableVertexAttribArray(4);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindVertexArray(0);
	}
	glBindVertexArray(planeVAO);
	glDrawArrays(GL_TRIANGLES, 0, 6);
	glBindVertexArray(0);
}

/* This function loads a texture from file. Note: texture loading functions like these are usually managed by a 'Resource Manager' that manages all resources (like textures, models, audio). For learning purposes we'll just define it as a utility function.*/
GLuint loadTexture(GLchar* path)
{
	int textureWidth, textureHeight;
	unsigned char* image = SOIL_load_image(path, &textureWidth, &textureHeight, 0, SOIL_LOAD_RGB);
	GLuint textureID;
	glGenTextures(1, &textureID); //generate texture object
	glBindTexture(GL_TEXTURE_2D, textureID); //sets the given texture object as the currently active texture object
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); // set WRAP option
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); // set WRAP option
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR); //set texture filtering option
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);//set texture filtering option
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, textureWidth, textureHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, image); //generates the texture image on the currently bound texture object at the active texture unit
	glGenerateMipmap(GL_TEXTURE_2D); //generate mipmaps for a specified texture target
	SOIL_free_image_data(image);
	glBindTexture(GL_TEXTURE_2D, 0);
	/////////////////////////////////////////////////////////////////////////////////////
	return textureID;
}

// Moves/alters the camera positions based on user input
void do_movement()
{
	//Camera controls
	if (keys[GLFW_KEY_W])
		camera.ProcessKeyboard(FORWARD, deltaTime);
	if (keys[GLFW_KEY_S])
		camera.ProcessKeyboard(BACKWARD, deltaTime);
	if (keys[GLFW_KEY_A])
		camera.ProcessKeyboard(LEFT, deltaTime);
	if (keys[GLFW_KEY_D])
		camera.ProcessKeyboard(RIGHT, deltaTime);

}

// Is called whenever a key is pressed/released via GLFW
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode)
{
	// When a user presses the escape key, we set the WindowShouldClose property to true, closing the application
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GL_TRUE); //sets the value of the close flag of the specified window
	else if (key == GLFW_KEY_SPACE && action == GLFW_PRESS)
	{
		inTangentSpace = !inTangentSpace;
		std::cout << (inTangentSpace ? "Tangent space ON" : "Tangent space OFF") << std::endl;
	}
	else if (key == GLFW_KEY_N && action == GLFW_PRESS)
	{
		normalMapping = !normalMapping;
		if (!normalMapping)
		{
			inTangentSpace = false;
			std::cout << ("Normal mapping OFF") << std::endl;
			std::cout << "Tangent space OFF" << std::endl;
		}
		else
		{
			std::cout << "Normal mapping ON" << std::endl;
			std::cout << (inTangentSpace ? "Tangent space ON" : "Tangent space OFF") << std::endl;
		}

	}
	else if (action == GLFW_PRESS)
		keys[key] = true;
	else if (action == GLFW_RELEASE)
		keys[key] = false;
	//std::cout << cameraPos.x << " " << cameraPos.y << " " << cameraPos.z << " " <<std::endl;
}

// Is called when the cursor is moved
void mouse_callback(GLFWwindow* window, double xpos, double ypos)
{
	/*If you would now run the code you will notice that the camera makes a large sudden jump whenever the window first receives focus of your mouse cursor.
	The cause for the sudden jump is that as soon as your cursor enters the window the mouse callback function is called with an xpos and ypos position equal to the location your mouse entered the screen.
	This is usually a position that is quite a distance away from the center of the screen resulting in large offsets and thus a large movement jump.
	We can circumvent this issue by simply defining a global bool variable to check if this is the first time we receive mouse input and if so, we first update the initial mouse positions to the new xpos and ypos values; the resulting mouse movements will then use the entered mouse's position coordinates to calculate its offsets:*/
	if (firstMouse)
	{
		lastX = xpos;
		lastY = ypos;
		firstMouse = false;
	}
	//offset movement between the last and current frame
	GLfloat xoffset = xpos - lastX;
	GLfloat yoffset = lastY - ypos; //Reversed since y-coordinates range from bottom to top
	lastX = xpos;
	lastY = ypos;
	camera.ProcessMouseMovement(xoffset, yoffset);
	//std::cout << xpos << " " << ypos << std::endl;
}

//is called when a scrolling device is used, such as a mouse wheel or scrolling area of a touchpad.
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset)
{
	camera.ProcessMouseScroll(yoffset);
}