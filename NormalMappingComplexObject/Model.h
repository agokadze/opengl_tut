#pragma once

#include <vector>
#include <GL/glew.h>

#include <assimp/scene.h>

#include "Shader.h"
#include "Mesh.h"

class Model
{
public:
	//Functions
	Model(GLchar* path, bool loadTangent = false); //Constructor, expects a filepath to a 3D model.
	void Draw(Shader &shader, GLboolean instanced = false, GLuint amount = 0);// Draws the model, and thus all its meshes
	std::vector<Mesh> meshes;
private:
	//Model data

	std::string directory;
	std::vector<Texture> textures_loaded;// Stores all the textures loaded so far, optimization to make sure textures aren't loaded more than once.
	bool loadTangent;

	//Functions
	void loadModel(std::string path);// Loads a model with supported ASSIMP extensions from file and stores the resulting meshes in the meshes vector.
	void processNode(aiNode* node, const aiScene* scene);// Processes a node in a recursive fashion. Processes each individual mesh located at the node and repeats this process on its children nodes (if any).
	Mesh processMesh(aiMesh* mesh, const aiScene* scene);
	std::vector<Texture> loadMaterialTextures(aiMaterial* mat, aiTextureType type, std::string typeName);// Checks all material textures of a given type and loads the textures if they're not loaded yet. The required info is returned as a Texture struct.
};